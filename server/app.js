const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

app.use(bodyParser.json());

require("./model/Employee");

const mongoUri =
  "mongodb+srv://<username>:<password>@cluster0.fzr1b.mongodb.net/<dbname>?retryWrites=true&w=majority";

mongoose.connect(mongoUri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

mongoose.connection.on("connected", () => {
  console.log("Connected To Mongo DB");
});
mongoose.connection.on("error", (err) => {
  console.log(`Mongo DB Not Connected ${err}`);
});

const Employee = mongoose.model("employee");

app.get("/", (req, res) => {
  Employee.find()
    .then((e) => res.send(e))
    .catch((e) => res.send(e));
});

app.post("/send-data", (req, res) => {
  const { name, email, phone, salary, position, picture } = req.body;

  console.log(name, email, phone, salary, position, picture);
  const employee = new Employee({
    name: name,
    email: email,
    phone: phone,
    salary: salary,
    position: position,
    picture: picture,
  });

  employee
    .save()
    .then((e) => res.send(`posted  ${e}`))
    .catch((e) => res.send(`posted  ${e}`));
});

app.post("/delete", (req, res) => {
  Employee.findByIdAndRemove(req.body.id)
    .then((e) => {
      res.send(`deleted response  ${e}`);
    })
    .catch((e) => res.send(`deleted response  ${e}`));
});

app.post("/update", (req, res) => {
  const { id, name, email, phone, salary, position, picture } = req.body;
  Employee.findByIdAndUpdate(id, {
    name: name,
    email: email,
    phone: phone,
    salary: salary,
    position: position,
    picture: picture,
  })
    .then((e) => {
      res.send(`updated response ${e}`);
    })
    .catch((e) => res.send(`updated response ${e}`));
});

app.listen(4000, () => {
  console.log("server running on port 4000");
});
