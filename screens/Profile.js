import React from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  Linking,
  Platform,
  Alert,
} from "react-native";
import { Title, Card, Button } from "react-native-paper";
import { LinearGradient } from "expo-linear-gradient";
import { MaterialIcons, Entypo } from "@expo/vector-icons";
import axios from "axios";

export const Profile = (props) => {
  const {
    _id,
    name,
    picture,
    phone,
    salary,
    email,
    position,
  } = props.route.params.item;

  const deleteEmployee = () => {
    axios
      .post("http://10.0.2.2:4000/delete", { id: _id })
      .then(() => {
        Alert.alert(`deleted sucessfully`);
        props.navigation.navigate("Home");
      })
      .catch((e) => Alert.alert(` some error ${e}`));
  };

  const openDial = () => {
    if (Platform.OS === "android") {
      Linking.openURL(`tel:${phone}`);
    } else {
    }
    Linking.openURL(`telprompt:${phone}`);
  };
  return (
    <View style={styles.root}>
      <LinearGradient
        colors={["#34918f", "#9fdddc"]}
        style={{ height: "20%" }}
      />
      <View style={{ alignItems: "center" }}>
        <Image
          style={{
            width: 140,
            height: 140,
            borderRadius: 140 / 2,
            marginTop: -60,
          }}
          source={{
            uri: picture,
          }}
        />
      </View>
      <View style={{ alignItems: "center", margin: 20 }}>
        <Title>{name}</Title>
        <Text>{position}</Text>
      </View>
      <Card
        style={styles.profileCard}
        onPress={() => Linking.openURL(`mailto:${email}`)}
      >
        <View style={styles.profileCardContent}>
          <MaterialIcons name="email" size={32} color="#9fdddc" />
          <Text style={styles.profileCardText}>{email}</Text>
        </View>
      </Card>
      <Card style={styles.profileCard} onPress={() => openDial()}>
        <View style={styles.profileCardContent}>
          <Entypo name="phone" size={32} color="#9fdddc" />
          <Text style={styles.profileCardText}>{phone}</Text>
        </View>
      </Card>
      <Card style={styles.profileCard}>
        <View style={styles.profileCardContent}>
          <MaterialIcons name="attach-money" size={32} color="#9fdddc" />
          <Text style={styles.profileCardText}>{salary}</Text>
        </View>
      </Card>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-around",
          padding: 10,
        }}
      >
        <Button
          style={styles.inputBox}
          icon="account-edit"
          mode="contained"
          onPress={() => {
            props.navigation.navigate("Create", {
              _id,
              name,
              picture,
              phone,
              salary,
              email,
              position,
            });
          }}
          theme={theme}
        >
          Edit
        </Button>
        <Button
          style={styles.inputBox}
          icon="delete"
          mode="contained"
          onPress={() => deleteEmployee()}
          theme={theme}
        >
          Remove
        </Button>
      </View>
    </View>
  );
};

const theme = { colors: { primary: "blue" } };

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  profileCard: {
    margin: 3,
  },
  profileCardContent: {
    flexDirection: "row",
  },
  profileCardText: {
    fontSize: 18,
    marginTop: 4,
    marginLeft: 10,
  },
});
