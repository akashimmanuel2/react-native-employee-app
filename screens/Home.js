import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  ActivityIndicator,
  Alert,
} from "react-native";
import { Card, FAB } from "react-native-paper";
import axios from "axios";

export const Home = (props) => {
  const [Data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    axios
      .get("http://10.0.2.2:4000/")
      .then((e) => {
        setData(e.data);
        setLoading(false);
      })
      .catch((e) => Alert.alert(` some error ${e}`));
  };

  const renderList = ({ item }) => {
    return (
      <Card
        style={styles.card1}
        onPress={() => props.navigation.navigate("Profile", { item: item })}
      >
        <View style={styles.card1View}>
          <Image
            style={{ width: 60, height: 60, borderRadius: 30 }}
            source={{
              uri: item.picture,
            }}
          />
          <View style={{ marginLeft: 10 }}>
            <Text style={styles.text}>{item.name}</Text>
            <Text style={styles.text}>{item.position}</Text>
          </View>
        </View>
      </Card>
    );
  };

  return (
    <View style={styles.root}>
      {loading ? (
        <ActivityIndicator size="large" color="#0000ff" />
      ) : (
        <>
          <FlatList
            data={Data}
            keyExtractor={(e) => `${e._id}`}
            renderItem={renderList}
            onRefresh={() => fetchData()}
            refreshing={loading}
          />
          <FAB
            style={styles.fab}
            small={false}
            icon="plus"
            onPress={() => props.navigation.navigate("Create")}
            theme={{ colors: { accent: "#34918f" } }}
          />
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  card1: {
    margin: 5,
    padding: 5,
  },
  card1View: {
    flexDirection: "row",
    padding: 5,
  },
  text: {
    fontSize: 20,
  },
  fab: {
    position: "absolute",
    margin: 16,
    right: 0,
    bottom: 0,
  },
});
