import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Modal,
  Alert,
  KeyboardAvoidingView,
} from "react-native";
import { TextInput, Button } from "react-native-paper";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import axios from "axios";

export const CreateEmployee = (props) => {
  const getDetails = (key) => {
    if (props.route.params) {
      switch (key) {
        case "name":
          return props.route.params.name;
        case "phone":
          return props.route.params.phone;
        case "email":
          return props.route.params.email;
        case "salary":
          return props.route.params.salary;
        case "picture":
          return props.route.params.picture;
        case "position":
          return props.route.params.position;
      }
    }
    return "";
  };

  const [name, setName] = useState(getDetails("name"));
  const [phone, setPhone] = useState(getDetails("phone"));
  const [email, setEmail] = useState(getDetails("email"));
  const [salary, setSalary] = useState(getDetails("salary"));
  const [picture, setPicture] = useState(getDetails("picture"));
  const [position, setPosition] = useState(getDetails("position"));
  const [modal, setModal] = useState(false);
  const [enableShift, setEnableShift] = useState(false);

  const pickFromGallery = async () => {
    const { granted } = await Permissions.askAsync(Permissions.MEDIA_LIBRARY);
    if (granted) {
      let data = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
      });
      if (!data.cancelled) {
        let newFile = {
          uri: data.uri,
          type: `test/${data.uri.split(".")[1]}`,
          name: `test.${data.uri.split(".")[1]}`,
        };
        handleUpload(newFile);
      }
    } else {
      Alert.alert("You need to give us permission to work");
    }
  };

  const pickFromCamera = async () => {
    const { granted } = await Permissions.askAsync(Permissions.MEDIA_LIBRARY);
    if (granted) {
      let data = await ImagePicker.launchCameraAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
      });
      if (!data.cancelled) {
        let newFile = {
          uri: data.uri,
          type: `test/${data.uri.split(".")[1]}`,
          name: `test.${data.uri.split(".")[1]}`,
        };
        handleUpload(newFile);
      }
    } else {
      Alert.alert("You need to give us permission to work");
    }
  };

  const handleUpload = (image) => {
    const data = new FormData();
    data.append("file", image);
    data.append("upload_preset", "employeeApp");
    data.append("cloud_name", "dvt9ax3zy");
    axios
      .post("https://api.cloudinary.com/v1_1/<username>/image/upload", data)
      .then((e) => {
        setPicture(e.data.url);
        setModal(false);
      })
      .catch((e) => Alert.alert(` some error ${e}`));
  };

  const onSubmitData = () => {
    axios
      .post("http://10.0.2.2:4000/send-data", {
        name,
        email,
        phone,
        salary,
        position,
        picture,
      })
      .then((e) => {
        console.log(e.data);
        Alert.alert(`saved successfully`);
        props.navigation.navigate("Home");
      })
      .catch((e) => Alert.alert(` some error ${e}`));
  };

  const upDateDetails = () => {
    axios
      .post("http://10.0.2.2:4000/update", {
        id: props.route.params._id,
        name,
        email,
        phone,
        salary,
        position,
        picture,
      })
      .then((e) => {
        console.log(e.data);
        Alert.alert(`Updated successfully`);
        props.navigation.navigate("Home");
      })
      .catch((e) => Alert.alert(` some error ${e}`));
  };

  return (
    <KeyboardAvoidingView
      behavior="position"
      style={styles.root}
      enabled={enableShift}
    >
      <View>
        <TextInput
          style={styles.inputBox}
          label="Name"
          value={name}
          onChangeText={(text) => setName(text)}
          mode="flat"
          theme={theme}
          // onFocus={() => setEnableShift(false)}
        />
        <TextInput
          style={styles.inputBox}
          label="Phone No"
          value={phone}
          onChangeText={(text) => setPhone(text)}
          mode="flat"
          theme={theme}
          keyboardType="number-pad"
          // onFocus={() => setEnableShift(false)}
        />
        <TextInput
          style={styles.inputBox}
          label="Email"
          value={email}
          onChangeText={(text) => setEmail(text)}
          mode="flat"
          theme={theme}
        />
        <TextInput
          style={styles.inputBox}
          label="Position"
          value={position}
          onChangeText={(text) => setPosition(text)}
          mode="flat"
          theme={theme}
          // onFocus={() => setEnableShift(true)}
        />
        <TextInput
          style={styles.inputBox}
          label="Salary"
          value={salary}
          onChangeText={(text) => setSalary(text)}
          mode="flat"
          theme={theme}
          // onFocus={() => setEnableShift(true)}
        />

        <Button
          style={styles.inputBox}
          icon={picture === "" ? "upload" : "check"}
          mode="contained"
          onPress={() => setModal(true)}
          theme={theme}
        >
          Upload Image
        </Button>

        {props.route.params ? (
          <Button
            style={styles.inputBox}
            icon="content-save"
            mode="contained"
            onPress={() => upDateDetails()}
            theme={theme}
          >
            Update
          </Button>
        ) : (
          <Button
            style={styles.inputBox}
            icon="content-save"
            mode="contained"
            onPress={() => onSubmitData()}
            theme={theme}
          >
            Save
          </Button>
        )}

        <Modal
          animationType="slide"
          transparent
          visible={modal}
          onRequestClose={() => setModal(false)}
        >
          <View style={styles.modalView}>
            <View style={styles.modalButtonView}>
              <Button
                icon="camera"
                mode="contained"
                onPress={() => pickFromCamera()}
                theme={theme}
              >
                Camera
              </Button>
              <Button
                icon="image-area"
                mode="contained"
                onPress={() => pickFromGallery()}
                theme={theme}
              >
                Gallery
              </Button>
            </View>
            <Button onPress={() => setModal(false)} theme={theme}>
              Cancel
            </Button>
          </View>
        </Modal>
      </View>
    </KeyboardAvoidingView>
  );
};

const theme = { colors: { primary: "blue" } };

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  inputBox: {
    margin: 5,
  },
  modalView: { position: "absolute", bottom: 2, width: "100%" },
  modalButtonView: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10,
  },
});
