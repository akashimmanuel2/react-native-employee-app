import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, View } from "react-native";
import Constants from "expo-constants";

import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";

import { CreateEmployee } from "./screens/CreateEmployee";
import { Home } from "./screens/Home";
import { Profile } from "./screens/Profile";

const Stack = createStackNavigator();

export default function App() {
  const navigationTheme = {
    title: "Home",
    headerTintColor: "black",
    headerStyle: {
      backgroundColor: "White",
    },
  };

  return (
    <NavigationContainer>
      <View style={styles.container}>
        <StatusBar style="dark" />
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={Home}
            options={navigationTheme}
          />
          <Stack.Screen
            name="Create"
            component={CreateEmployee}
            options={{ ...navigationTheme, title: "Create Employee" }}
          />
          <Stack.Screen
            name="Profile"
            component={Profile}
            options={{ ...navigationTheme, title: "Profile" }}
          />
        </Stack.Navigator>
      </View>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    marginTop: Constants.statusBarHeight,
  },
});
